# !/bin/bash
echo "USAGE: ./build.sh (release|debug) (wechat|worker|web|node|shell)"

if [ x$1 == x'debug' ]
then
    echo "start debug build ..."
    MAKE_SWITCHS='CMAKE_BUILD_TYPE=Debug'
    EMCC_SWITCHS='-g4'
else
    echo "start release build ..."
    MAKE_SWITCHS='CMAKE_BUILD_TYPE=Release'
    EMCC_SWITCHS='-O3 --closure 1'
fi

TYPE_SWITCHS='wechat'
FILE_SWITCHS='wechat'
WASM_SWITCHS='1'

if [ x$2 != x'' ]
then
    TYPE_SWITCHS=$2
    FILE_SWITCHS=$2
fi

if [ ${TYPE_SWITCHS} == 'wechat' ]
then
    TYPE_SWITCHS='worker'    
    WASM_SWITCHS='1'
    PREJS_OPTION='--pre-js wechat-prejs.js'
    echo "start wechat build ..."
else
    echo "start ${TYPE_SWITCHS} build ..."
    PREJS_OPTION=''
fi

make clean
emcmake cmake -D ${MAKE_SWITCHS} -D BUILD_WASM=ON .
emcmake cmake .
emmake make
`emcc src/libchipmunk.so \
     ${EMCC_SWITCHS} \
     ${PREJS_OPTION} \
     -s WASM=${WASM_SWITCHS} \
     -s ENVIRONMENT=${TYPE_SWITCHS} \
     -s MODULARIZE=1 \
     -s RESERVED_FUNCTION_POINTERS=32 \
     -s EXTRA_EXPORTED_RUNTIME_METHODS="['ccall', 'cwrap', 'ALLOC_STACK', 'allocate', 'getValue', 'setValue', 'addFunction']" \
     -s EXPORTED_FUNCTIONS="[\
          '_cpBodyNew', \
          '_cpBodyNewStatic', \
          '_cpBodyNewKinematic', \
          '_cpBodyFree', \
          '_cpBodySetUserData', \
          '_cpBodyGetUserData', \
          '_chipmunk_cpBodyGetPosition', \
          '_chipmunk_cpBodySetPosition', \
          '_chipmunk_cpBodyGetVelocity', \
          '_chipmunk_cpBodySetVelocity', \
          '_cpBodyGetMass', \
          '_cpBodySetMass', \
          '_cpBodyGetAngle', \
          '_cpBodySetAngle', \
          '_cpBodySetAngularVelocity', \
          '_cpBodyGetAngularVelocity', \
          '_cpBodyGetType', \
          '_cpBodySetType', \
          '_cpBodyIsSleeping', \
          '_cpBodyActivate', \
          '_cpBodySleep', \
          '_chipmunk_cpBodyUpdateVelocity', \
          '_chipmunk_cpBodySetVelocityUpdateFunc', \
          '_cpBodyUpdatePosition', \
          '_cpBodySetPositionUpdateFunc', \
          '_chipmunk_cpBodyApplyForceAtWorldPoint', \
          '_chipmunk_cpBodyApplyForceAtLocalPoint', \
          '_chipmunk_cpBodyApplyImpulseAtWorldPoint', \
          '_chipmunk_cpBodyApplyImpulseAtLocalPoint', \
          '_cpSpaceNew', \
          '_cpSpaceContainsBody', \
          '_cpSpaceContainsShape', \
          '_cpSpaceAddBody', \
          '_cpSpaceRemoveBody', \
          '_cpSpaceReindexShapesForBody', \
          '_cpSpaceAddShape', \
          '_cpSpaceRemoveShape', \
          '_cpSpaceAddConstraint', \
          '_cpSpaceRemoveConstraint', \
          '_cpSpaceStep', \
          '_cpSpaceSetIterations', \
          '_cpSpaceGetIterations', \
          '_cpSpaceSetIdleSpeedThreshold', \
          '_cpSpaceGetIdleSpeedThreshold', \
          '_cpSpaceSetSleepTimeThreshold', \
          '_cpSpaceGetSleepTimeThreshold', \
          '_cpSpaceSetCollisionSlop', \
          '_cpSpaceGetCollisionSlop', \
          '_cpSpaceSetCollisionBias', \
          '_cpSpaceGetCollisionBias', \
          '_chipmunk_cpSpaceSetGravity', \
          '_chipmunk_cpSpaceGetGravity', \
          '_cpSpaceSetDamping', \
          '_cpSpaceGetDamping', \
          '_cpSpaceSetUserData', \
          '_cpSpaceGetUserData', \
          '_cpSpaceAddPostStepCallback', \
          '_chipmunk_cpSpaceAddCollisionHandler', \
          '_chipmunk_cpSpacePointQuery', \
          '_chipmunk_cpSpaceShapeQuery', \
          '_cpSpaceFree', \
          '_chipmunk_cpSegmentShapeNew', \
          '_chipmunk_cpSegmentShapeGetA', \
          '_chipmunk_cpSegmentShapeGetB', \
          '_chipmunk_cpCircleShapeNew', \
          '_chipmunk_cpPolyShapeNewRaw', \
          '_cpCircleShapeGetRadius', \
          '_cpCircleShapeSetRadius', \
          '_chipmunk_cpCircleShapeGetOffset', \
          '_chipmunk_cpCircleShapeSetOffset', \
          '_cpShapeGetSpace', \
          '_cpShapeSetBody', \
          '_cpShapeGetBody', \
          '_chipmunk_cpShapeSetFilter', \
          '_chipmunk_cpShapeGetFilter', \
          '_cpShapeSetCollisionType', \
          '_cpShapeGetCollisionType', \
          '_cpShapeGetElasticity', \
          '_cpShapeSetElasticity', \
          '_cpShapeGetFriction', \
          '_cpShapeSetFriction', \
          '_cpShapeGetSensor', \
          '_cpShapeSetSensor', \
          '_cpShapeFree', \
          '_chipmunk_cpSlideJointNew', \
          '_chipmunk_cpPivotJointNew2', \
          '_cpConstraintSetUserData', \
          '_cpConstraintGetUserData', \
          '_cpConstraintGetMaxBias', \
          '_cpConstraintSetMaxBias', \
          '_cpConstraintGetMaxForce', \
          '_cpConstraintSetMaxForce', \
          '_cpConstraintGetErrorBias', \
          '_cpConstraintSetErrorBias', \
          '_cpConstraintFree', \
          '_chipmunk_cpArbiterGetShapes', \
          '_chipmunk_cpArbiterGetBodies', \
          '_chipmunk_cpArbiterGetNormal', \
          '_cpArbiterIgnore', \
          '_cpArbiterSetRestitution', \
          '_cpArbiterSetFriction', \
          '_chipmunk_cpArbiterSetSurfaceVelocity', \
          '_chipmunk_cpSpaceDebugDraw' \
      ]" \
     -o src/libchipmunk.${FILE_SWITCHS}.js`

if [ x$FILE_SWITCHS == x'node' ]
then
    `gsed -i '1i\/** @nocompile */' src/libchipmunk.${FILE_SWITCHS}.js`
fi

`cp -rf src/libchipmunk.${FILE_SWITCHS}.js ../chipmunk-wasm/src/`

if [ x$1 == x'debug' ]
then
   `cp -rf src/libchipmunk.${FILE_SWITCHS}.js.map ../chipmunk-wasm/src/`
fi
`cp -rf src/libchipmunk.${FILE_SWITCHS}.wasm ../chipmunk-wasm/src/`
