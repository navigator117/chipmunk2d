#ifndef LIBCHIPMUNK_H
#define LIBCHIPMUNK_H

#ifdef BUILD_WASM

CP_EXPORT void
chipmunk_cpBodyGetPosition(const cpBody *body, cpFloat* result);

CP_EXPORT void
chipmunk_cpBodySetPosition(cpBody *body, cpFloat positionX, cpFloat positionY);

CP_EXPORT void
chipmunk_cpBodyGetVelocity(const cpBody *body, cpFloat* result);

CP_EXPORT void
chipmunk_cpBodySetVelocity(cpBody *body, cpFloat velocityX, cpFloat velocityY);

CP_EXPORT void
chipmunk_cpBodyUpdateVelocity(cpBody *body, cpFloat gravityX, cpFloat gravityY, cpFloat damping, cpFloat dt);

typedef void (*chipmunk_cpBodyVelocityFunc)(cpBody *body, cpFloat gravityX, cpFloat gravityY, cpFloat damping, cpFloat dt);

CP_EXPORT void
chipmunk_cpBodySetVelocityUpdateFunc(cpBody *body, chipmunk_cpBodyVelocityFunc velocityFunc);

CP_EXPORT void
chipmunk_cpBodyApplyForceAtWorldPoint(cpBody *body, cpFloat forceX, cpFloat forceY, cpFloat pointX, cpFloat pointY);

CP_EXPORT void
chipmunk_cpBodyApplyForceAtLocalPoint(cpBody *body, cpFloat forceX, cpFloat forceY, cpFloat pointX, cpFloat pointY);

CP_EXPORT void
chipmunk_cpBodyApplyImpulseAtWorldPoint(cpBody *body, cpFloat impulseX, cpFloat impulseY, cpFloat pointX, cpFloat pointY);

CP_EXPORT void
chipmunk_cpBodyApplyImpulseAtLocalPoint(cpBody *body, cpFloat impulseX, cpFloat impulseY, cpFloat pointX, cpFloat pointY);

CP_EXPORT void
chipmunk_cpSpaceGetGravity(const cpSpace *space, cpFloat* result);

CP_EXPORT void
chipmunk_cpSpaceSetGravity(cpSpace *space, cpFloat gravityX, cpFloat gravityY);

CP_EXPORT cpCollisionHandler*
chipmunk_cpSpaceAddCollisionHandler(cpSpace *space,
                                    cpCollisionType a,
                                    cpCollisionType b,
                                    cpCollisionType collisionKey,
                                    cpCollisionBeginFunc beginFunc,
                                    cpCollisionPreSolveFunc preSolveFunc,
                                    cpCollisionPostSolveFunc postSolveFunc,
                                    cpCollisionSeparateFunc separateFunc);

typedef void (*chipmunk_cpSpacePointQueryFunc)(cpShape *shape, cpFloat pointX, cpFloat pointY, cpFloat distance, cpFloat gradientX, cpFloat gradientY);

CP_EXPORT void
chipmunk_cpSpacePointQuery(cpSpace *space,
                           cpFloat pointX, cpFloat pointY,
                           cpFloat maxDistance,
                           cpGroup filterGroup, cpBitmask filterCategories, cpBitmask filterMask,
                           chipmunk_cpSpacePointQueryFunc callback);

typedef void (*chipmunk_cpSpaceShapeQueryFunc)(cpShape *shape, cpContactPointSet* points);

CP_EXPORT void
chipmunk_cpSpaceShapeQuery(cpSpace *space, cpShape *shape, chipmunk_cpSpaceShapeQueryFunc callback);

CP_EXPORT cpShape*
chipmunk_cpSegmentShapeNew(cpBody *body, cpFloat aX, cpFloat aY, cpFloat bX, cpFloat bY, cpFloat radius);

CP_EXPORT void
chipmunk_cpSegmentShapeGetA(const cpShape *body, cpFloat* result);

CP_EXPORT void
chipmunk_cpSegmentShapeGetB(const cpShape *body, cpFloat* result);

CP_EXPORT cpShape*
chipmunk_cpCircleShapeNew(cpBody *body, cpFloat radius, cpFloat offsetX, cpFloat offsetY);

CP_EXPORT cpShape*
chipmunk_cpPolyShapeNewRaw(cpBody *body, int numOfVertex, const cpFloat *buffers, cpFloat radius);

CP_EXPORT void
chipmunk_cpCircleShapeGetOffset(const cpShape *circleShape, cpFloat* result);

CP_EXPORT void
chipmunk_cpCircleShapeSetOffset(cpShape *circleShape, cpFloat offsetX, cpFloat offsetY);

CP_EXPORT void
chipmunk_cpShapeGetFilter(const cpShape *shape, uint32_t* result);

CP_EXPORT void
chipmunk_cpShapeSetFilter(cpShape *shape, cpGroup filterGroup, cpBitmask filterCategories, cpBitmask filterMask);

CP_EXPORT cpConstraint*
chipmunk_cpSlideJointNew(cpBody *a, cpBody *b, cpFloat anchorAX, cpFloat anchorAY, cpFloat anchorBX, cpFloat anchorBY, cpFloat min, cpFloat max);

CP_EXPORT cpConstraint*
chipmunk_cpPivotJointNew2(cpBody *a, cpBody *b, cpFloat anchorAX, cpFloat anchorAY, cpFloat anchorBX, cpFloat anchorBY);

CP_EXPORT void
chipmunk_cpArbiterGetShapes(const cpArbiter* arbiter, cpShape* result[]);

CP_EXPORT void
chipmunk_cpArbiterGetBodies(const cpArbiter* arbiter, cpBody* result[]);

CP_EXPORT void
chipmunk_cpArbiterGetNormal(const cpArbiter* arbiter, cpFloat* result);

CP_EXPORT void
chipmunk_cpArbiterSetSurfaceVelocity(cpArbiter *arbiter, cpFloat surfaceVelocityX, cpFloat surfaceVelocityY);

typedef void (*chipmunk_cpSpaceDebugDrawSegmentFunc)(cpSpace *space,
                                                     cpFloat aX,
                                                     cpFloat aY,
                                                     cpFloat bX,
                                                     cpFloat bY,
                                                     cpFloat colorR,
                                                     cpFloat colorG,
                                                     cpFloat colorB);

CP_EXPORT void
chipmunk_cpSpaceDebugDraw(cpSpace* space, cpSpaceDebugDrawFlags flags, chipmunk_cpSpaceDebugDrawSegmentFunc drawSegmentFunc);

#endif //  BUILD_WASM

#endif // LIBCHIPMUNK_H
