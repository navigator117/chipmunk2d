#include <alloca.h>
#include "chipmunk/chipmunk_private.h"
#include <chipmunk/chipmunk_unsafe.h>
#include <chipmunk/libchipmunk.h>

#ifdef BUILD_WASM

void
chipmunk_cpBodyGetPosition(const cpBody *body, cpFloat* result)
{
	cpVect position = cpBodyGetPosition(body);
    result[0] = position.x;
    result[1] = position.y;
}

void
chipmunk_cpBodySetPosition(cpBody *body, cpFloat positionX, cpFloat positionY)
{
    cpVect position = cpvzero;
    position.x = positionX;
    position.y = positionY;
    cpBodySetPosition(body, position);
}

void
chipmunk_cpBodyGetVelocity(const cpBody *body, cpFloat* result)
{
	cpVect velocity = cpBodyGetVelocity(body);
    result[0] = velocity.x;
    result[1] = velocity.y;
}

void
chipmunk_cpBodySetVelocity(cpBody *body, cpFloat velocityX, cpFloat velocityY)
{
    cpVect velocity = cpvzero;
    velocity.x = velocityX;
    velocity.y = velocityY;
    cpBodySetVelocity(body, velocity);
}

void
chipmunk_cpBodyUpdateVelocity(cpBody *body, cpFloat gravityX, cpFloat gravityY, cpFloat damping, cpFloat dt)
{
    cpVect gravity = cpvzero;
    gravity.x = gravityX;
    gravity.y = gravityY;
    cpBodyUpdateVelocity(body, gravity, damping, dt);
}

static void
chipmunk_cpBodyVelocityUpdateCallback(cpBody *body, cpVect gravity, cpFloat damping, cpFloat dt)
{
    chipmunk_cpBodyVelocityFunc velocityFunc = (chipmunk_cpBodyVelocityFunc)cpBodyGetUserData(body);
    if (velocityFunc != NULL) {
        velocityFunc(body, gravity.x, gravity.y, damping, dt);
    }
}

void
chipmunk_cpBodySetVelocityUpdateFunc(cpBody *body, chipmunk_cpBodyVelocityFunc velocityFunc)
{
    if (velocityFunc != NULL) {
        cpBodySetUserData(body, (cpDataPointer)velocityFunc);
        cpBodySetVelocityUpdateFunc(body, chipmunk_cpBodyVelocityUpdateCallback);
    }
    else {
        cpBodySetVelocityUpdateFunc(body, cpBodyUpdateVelocity);
    }
}

void
chipmunk_cpBodyApplyForceAtWorldPoint(cpBody *body, cpFloat forceX, cpFloat forceY, cpFloat pointX, cpFloat pointY)
{
    cpVect force = cpvzero, point = cpvzero;
    force.x = forceX;
    force.y = forceY;
    point.x = pointX;
    point.y = pointY;
    cpBodyApplyForceAtWorldPoint(body, force, point);
}

void
chipmunk_cpBodyApplyForceAtLocalPoint(cpBody *body, cpFloat forceX, cpFloat forceY, cpFloat pointX, cpFloat pointY)
{
    cpVect force = cpvzero, point = cpvzero;
    force.x = forceX;
    force.y = forceY;
    point.x = pointX;
    point.y = pointY;
    cpBodyApplyForceAtLocalPoint(body, force, point);
}

void
chipmunk_cpBodyApplyImpulseAtWorldPoint(cpBody *body, cpFloat impulseX, cpFloat impulseY, cpFloat pointX, cpFloat pointY)
{
    cpVect impulse = cpvzero, point = cpvzero;
    impulse.x = impulseX;
    impulse.y = impulseY;
    point.x = pointX;
    point.y = pointY;
    cpBodyApplyImpulseAtWorldPoint(body, impulse, point);
}

void
chipmunk_cpBodyApplyImpulseAtLocalPoint(cpBody *body, cpFloat impulseX, cpFloat impulseY, cpFloat pointX, cpFloat pointY)
{
    cpVect impulse = cpvzero, point = cpvzero;
    impulse.x = impulseX;
    impulse.y = impulseY;
    point.x = pointX;
    point.y = pointY;
    cpBodyApplyImpulseAtLocalPoint(body, impulse, point);
}

void
chipmunk_cpSpaceGetGravity(const cpSpace *space, cpFloat* result)
{
	cpVect gravity = cpSpaceGetGravity(space);
    result[0] = gravity.x;
    result[1] = gravity.y;
}

void
chipmunk_cpSpaceSetGravity(cpSpace *space, cpFloat gravityX, cpFloat gravityY)
{
    cpVect gravity = cpvzero;
    gravity.x = gravityX;
    gravity.y = gravityY;
    cpSpaceSetGravity(space, gravity);
}

cpCollisionHandler*
chipmunk_cpSpaceAddCollisionHandler(cpSpace *space,
                                    cpCollisionType a,
                                    cpCollisionType b,
                                    cpCollisionType collisionKey,
                                    cpCollisionBeginFunc beginFunc,
                                    cpCollisionPreSolveFunc preSolveFunc,
                                    cpCollisionPostSolveFunc postSolveFunc,
                                    cpCollisionSeparateFunc separateFunc)
{
    cpCollisionHandler* collisionHandler = cpSpaceAddCollisionHandler(space, a, b);
    if (collisionHandler != NULL) {
        if (beginFunc != NULL) {
            collisionHandler->beginFunc = beginFunc;
        }
        if (preSolveFunc != NULL) {
            collisionHandler->preSolveFunc = preSolveFunc;
        }
        if (postSolveFunc != NULL) {
            collisionHandler->postSolveFunc = postSolveFunc;
        }
        if (separateFunc != NULL) {
            collisionHandler->separateFunc = separateFunc;
        }
        collisionHandler->userData = (cpDataPointer)collisionKey;
    }
    return collisionHandler;
}

static void
chipmunk_cpSpacePointQueryCallback(cpShape *shape, cpVect point, cpFloat distance, cpVect gradient, void *data)
{
    chipmunk_cpSpacePointQueryFunc callback = (chipmunk_cpSpacePointQueryFunc)data;
    if (callback != NULL) {
        callback(shape, point.x, point.y, distance, gradient.x, gradient.y);
    }
}

void
chipmunk_cpSpacePointQuery(cpSpace *space,
                           cpFloat pointX, cpFloat pointY,
                           cpFloat maxDistance,
                           cpGroup filterGroup, cpBitmask filterCategories, cpBitmask filterMask,
                           chipmunk_cpSpacePointQueryFunc callback)
{
    cpVect point = cpvzero;
    cpShapeFilter filter = { .group = CP_NO_GROUP, .categories = CP_ALL_CATEGORIES, .mask = CP_ALL_CATEGORIES };
    point.x = pointX;
    point.y = pointY;
    filter.group = filterGroup;
    filter.categories = filterCategories;
    filter.mask = filterMask;
    cpSpacePointQuery(space, point, maxDistance, filter, chipmunk_cpSpacePointQueryCallback, (cpDataPointer)callback);
}

typedef struct cpSpaceDebugDrawContext {
    chipmunk_cpSpaceDebugDrawSegmentFunc drawSegmentFunc;
    cpSpace* space;
} cpSpaceDebugDrawContext;

static void
chipmunk_cpSpaceDebugDrawCircleCallback(cpVect position,
                                        cpFloat angle,
                                        cpFloat radius,
                                        cpSpaceDebugColor outlineColor,
                                        cpSpaceDebugColor fillColor,
                                        cpDataPointer data)
{
    cpSpaceDebugDrawContext* context = (cpSpaceDebugDrawContext*)data;
    const int numOfSegments = 32;
    const cpFloat delta = M_PI * 2.0 / numOfSegments;
    for (int i=0; i<numOfSegments; ++i) {
        cpVect a = cpvadd(position, cpvrotate(cpv(radius, 0), cpvforangle(((i)     % numOfSegments) * delta)));
        cpVect b = cpvadd(position, cpvrotate(cpv(radius, 0), cpvforangle(((i + 1) % numOfSegments) * delta)));
        context->drawSegmentFunc(context->space,
                                 a.x,
                                 a.y,
                                 b.x,
                                 b.y,
                                 outlineColor.r,
                                 outlineColor.g,
                                 outlineColor.b);
    }
}

static void
chipmunk_cpSpaceDebugDrawSegmentCallback(cpVect a,
                                         cpVect b,
                                         cpSpaceDebugColor color,
                                         cpDataPointer data)
{
    cpSpaceDebugDrawContext* context = (cpSpaceDebugDrawContext*)data;
    context->drawSegmentFunc(context->space,
                             a.x,
                             a.y,
                             b.x,
                             b.y,
                             color.r,
                             color.g,
                             color.b);
}

static void
chipmunk_cpSpaceDebugDrawFatSegmentCallback(cpVect a,
                                            cpVect b,
                                            cpFloat radius,
                                            cpSpaceDebugColor outlineColor,
                                            cpSpaceDebugColor fillColor,
                                            cpDataPointer data)
{
    cpSpaceDebugDrawContext* context = (cpSpaceDebugDrawContext*)data;
    cpFloat angle = cpvtoangle(cpvsub(b, a));
    cpFloat deltaX = radius * cpfsin(angle);
    cpFloat deltaY = radius * cpfcos(angle);
    context->drawSegmentFunc(context->space,
                             a.x,
                             a.y + deltaY,
                             b.x,
                             b.y + deltaY,
                             outlineColor.r,
                             outlineColor.g,
                             outlineColor.b);
    context->drawSegmentFunc(context->space,
                             a.x + deltaX,
                             a.y,
                             b.x + deltaX,
                             b.y,
                             outlineColor.r,
                             outlineColor.g,
                             outlineColor.b);        
    context->drawSegmentFunc(context->space,
                             a.x,
                             a.y - deltaY,
                             b.x,
                             b.y - deltaY,
                             outlineColor.r,
                             outlineColor.g,
                             outlineColor.b);
    context->drawSegmentFunc(context->space,
                             a.x - deltaX,
                             a.y,
                             b.x - deltaX,
                             b.y,
                             outlineColor.r,
                             outlineColor.g,
                             outlineColor.b);
}

static void
chipmunk_cpSpaceDebugDrawPolygonCallback(int count,
                                         const cpVect *verts,
                                         cpFloat radius,
                                         cpSpaceDebugColor outlineColor,
                                         cpSpaceDebugColor fillColor,
                                         cpDataPointer data)
{
    cpSpaceDebugDrawContext* context = (cpSpaceDebugDrawContext*)data;
    for (int i=0; i<count; ++i) {
        const cpVect a = verts[(i)     % count];
        const cpVect b = verts[(i + 1) % count];
        context->drawSegmentFunc(context->space,
                                 a.x,
                                 a.y,
                                 b.x,
                                 b.y,
                                 outlineColor.r,
                                 outlineColor.g,
                                 outlineColor.b);
    }
}

static void
chipmunk_cpSpaceDebugDrawDotCallback(cpFloat size,
                                     cpVect position,
                                     cpSpaceDebugColor color,
                                     cpDataPointer data)
{
    cpSpaceDebugDrawContext* context = (cpSpaceDebugDrawContext*)data;
    const int numOfSegments = 6;
    const cpFloat delta = M_PI * 2.0 / numOfSegments;
    const cpFloat radius = size * 0.5;
    for (int i=0; i<numOfSegments; ++i) {
        cpVect a = cpvadd(position, cpvrotate(cpv(radius, 0), cpvforangle(((i)     % numOfSegments) * delta)));
        cpVect b = cpvadd(position, cpvrotate(cpv(radius, 0), cpvforangle(((i + 1) % numOfSegments) * delta)));
        context->drawSegmentFunc(context->space,
                                 a.x,
                                 a.y,
                                 b.x,
                                 b.y,
                                 color.r,
                                 color.g,
                                 color.b);
    }
}

cpSpaceDebugColor
chipmunk_cpSpaceDebugDrawColorForShapeCallback(cpShape* shape, cpDataPointer data)
{
    cpSpaceDebugColor red;
    red.r = 1.0;
    red.g = 0.0;
    red.b = 0.0;
    red.a = 1.0;
    return red;
}

void
chipmunk_cpSpaceDebugDraw(cpSpace *space, cpSpaceDebugDrawFlags flags, chipmunk_cpSpaceDebugDrawSegmentFunc drawSegmentFunc)
{
    cpSpaceDebugDrawOptions space_debug_draw_options;
    cpSpaceDebugDrawContext space_debug_draw_context;
    space_debug_draw_options.drawCircle = chipmunk_cpSpaceDebugDrawCircleCallback;
    space_debug_draw_options.drawSegment = chipmunk_cpSpaceDebugDrawSegmentCallback;
    space_debug_draw_options.drawFatSegment = chipmunk_cpSpaceDebugDrawFatSegmentCallback;
    space_debug_draw_options.drawPolygon = chipmunk_cpSpaceDebugDrawPolygonCallback;
    space_debug_draw_options.drawDot = chipmunk_cpSpaceDebugDrawDotCallback;
    space_debug_draw_options.flags = flags;
    space_debug_draw_options.shapeOutlineColor.r = 0.0;
    space_debug_draw_options.shapeOutlineColor.g = 0.0;
    space_debug_draw_options.shapeOutlineColor.b = 0.0;
    space_debug_draw_options.shapeOutlineColor.a = 1.0;
    space_debug_draw_options.colorForShape = chipmunk_cpSpaceDebugDrawColorForShapeCallback;
    space_debug_draw_options.constraintColor.r = 0.0;
    space_debug_draw_options.constraintColor.g = 1.0;
    space_debug_draw_options.constraintColor.b = 0.0;
    space_debug_draw_options.constraintColor.a = 1.0;
    space_debug_draw_options.collisionPointColor.r = 0.0;
    space_debug_draw_options.collisionPointColor.g = 0.0;
    space_debug_draw_options.collisionPointColor.b = 1.0;
    space_debug_draw_options.collisionPointColor.a = 1.0;
    space_debug_draw_context.space = space;
    space_debug_draw_context.drawSegmentFunc = drawSegmentFunc;
    space_debug_draw_options.data = (cpDataPointer)&space_debug_draw_context;
    cpSpaceDebugDraw(space, &space_debug_draw_options);
}

static void
chipmunk_cpSpaceShapeQueryCallback(cpShape *shape, cpContactPointSet *points, void* data)
{
    chipmunk_cpSpaceShapeQueryFunc callback = (chipmunk_cpSpaceShapeQueryFunc)data;
    if (callback != NULL) {
        callback(shape, points);
    }
}

void
chipmunk_cpSpaceShapeQuery(cpSpace *space, cpShape *shape, chipmunk_cpSpaceShapeQueryFunc callback)
{
    cpSpaceShapeQuery(space, shape, chipmunk_cpSpaceShapeQueryCallback, (cpDataPointer)callback);
}

cpShape*
chipmunk_cpSegmentShapeNew(cpBody *body, cpFloat aX, cpFloat aY, cpFloat bX, cpFloat bY, cpFloat radius)
{
    cpVect a = cpvzero, b = cpvzero;
    a.x = aX;
    a.y = aY;
    b.x = bX;
    b.y = bY;
    return cpSegmentShapeNew(body, a, b, radius);
}

void
chipmunk_cpSegmentShapeGetA(const cpShape *shape, cpFloat* result)
{
	cpVect a = cpSegmentShapeGetA(shape);
    result[0] = a.x;
    result[1] = a.y;
}

void
chipmunk_cpSegmentShapeGetB(const cpShape *shape, cpFloat* result)
{
	cpVect b = cpSegmentShapeGetB(shape);
    result[0] = b.x;
    result[1] = b.y;
}

cpShape*
chipmunk_cpCircleShapeNew(cpBody *body, cpFloat radius, cpFloat offsetX, cpFloat offsetY)
{
    cpVect offset = cpvzero;
    offset.x = offsetX;
    offset.y = offsetY;
    return cpCircleShapeNew(body, radius, offset);
}

cpShape*
chipmunk_cpPolyShapeNewRaw(cpBody *body, int numOfVertex, const cpFloat *buffers, cpFloat radius)
{
    uint32_t i = 0;
    cpVect* vertexs = alloca(numOfVertex * sizeof(cpVect));
    for (i=0; i<numOfVertex; ++i) {
        vertexs[i].x = buffers[i * 2 + 0];
        vertexs[i].y = buffers[i * 2 + 1];
    }
    return cpPolyShapeNewRaw(body, numOfVertex, vertexs, radius);
}

void
chipmunk_cpCircleShapeGetOffset(const cpShape *circleShape, cpFloat* result)
{
	cpVect offset = cpCircleShapeGetOffset(circleShape);
    result[0] = offset.x;
    result[1] = offset.y;
}

void
chipmunk_cpCircleShapeSetOffset(cpShape *circleShape, cpFloat offsetX, cpFloat offsetY)
{
    cpVect offset = cpvzero;
    offset.x = offsetX;
    offset.y = offsetY;
    cpCircleShapeSetOffset(circleShape, offset);
}

void
chipmunk_cpShapeGetFilter(const cpShape *shape, uint32_t* result)
{
    cpShapeFilter filter = CP_SHAPE_FILTER_NONE;    
    filter = cpShapeGetFilter(shape);
    result[0] = filter.group;
    result[1] = filter.categories;
    result[2] = filter.mask;
}

void
chipmunk_cpShapeSetFilter(cpShape *shape, cpGroup filterGroup, cpBitmask filterCategories, cpBitmask filterMask)
{
    cpShapeFilter filter = CP_SHAPE_FILTER_NONE;
    filter.group = filterGroup;
    filter.categories = filterCategories;
    filter.mask = filterMask;
    cpShapeSetFilter(shape, filter);    
}

cpConstraint*
chipmunk_cpSlideJointNew(cpBody *a, cpBody *b, cpFloat anchorAX, cpFloat anchorAY, cpFloat anchorBX, cpFloat anchorBY, cpFloat min, cpFloat max)
{
    cpVect anchorA = cpvzero, anchorB = cpvzero;
    anchorA.x = anchorAX;
    anchorA.y = anchorAY;
    anchorB.x = anchorBX;
    anchorB.y = anchorBY;
    return cpSlideJointNew(a, b, anchorA, anchorB, min, max);
}

cpConstraint*
chipmunk_cpPivotJointNew2(cpBody *a, cpBody *b, cpFloat anchorAX, cpFloat anchorAY, cpFloat anchorBX, cpFloat anchorBY)
{
    cpVect anchorA = cpvzero, anchorB = cpvzero;
    anchorA.x = anchorAX;
    anchorA.y = anchorAY;
    anchorB.x = anchorBX;
    anchorB.y = anchorBY;
    return cpPivotJointNew2(a, b, anchorA, anchorB);
}

void
chipmunk_cpArbiterGetShapes(const cpArbiter* arbiter, cpShape* result[])
{
    cpShape* a = NULL;
    cpShape* b = NULL;
    cpArbiterGetShapes(arbiter, &a, &b);
    result[0] = a;
    result[1] = b;
}

void
chipmunk_cpArbiterGetBodies(const cpArbiter* arbiter, cpBody* result[])
{
    cpBody* a = NULL;
    cpBody* b = NULL;
    cpArbiterGetBodies(arbiter, &a, &b);
    result[0] = a;
    result[1] = b;
}

void
chipmunk_cpArbiterGetNormal(const cpArbiter *arbiter, cpFloat* result)
{
	cpVect normal = cpArbiterGetNormal(arbiter);
    result[0] = normal.x;
    result[1] = normal.y;
}

void
chipmunk_cpArbiterSetSurfaceVelocity(cpArbiter *arbiter, cpFloat surfaceVelocityX, cpFloat surfaceVelocityY)
{
    cpVect surfaceVelocity = cpvzero;
    surfaceVelocity.x = surfaceVelocityX;
    surfaceVelocity.y = surfaceVelocityY;
    cpArbiterSetSurfaceVelocity(arbiter, surfaceVelocity);
}

#endif // BUILD_WASM
