# !/bin/bash
echo "USAGE: ./buildAll.sh (release|debug)"

if [ x$1 == x'debug' ]
then
    echo "start debug build ..."
    MAKE_SWITCHS='debug'

else
    echo "start release build ..."
    MAKE_SWITCHS='release'
fi

./build.sh ${MAKE_SWITCHS} wechat
./build.sh ${MAKE_SWITCHS} worker
./build.sh ${MAKE_SWITCHS} web
./build.sh ${MAKE_SWITCHS} node

